%--------------------------------------------------------
%   For perfusion calculation, take 1/3 k-space, 2X shots

clear
close all
clc

%----------------------------
%    Define the parameters

% System
gmax    = 5;                        % G/cm
smax    = 15;                       % G/cm/ms
T       = 2e-3;                     % gradient sample time

% Design Parameters
mat     = 32;                       % matrix size
fov     = 256;                      % mm
res     = fov/mat;                  % mm
kmax    = 5 / res;                  % [1/cm]

Ts              = 2e-3;                     % ms
bw_readout      = 1000/Ts;                  % Hz
readout_time    = 2.5;                      % ms
LEN             = floor( readout_time / Ts );


cone_types  = {'Johnson', 'Gurney', 'improvedGurney', 'VDGurney', 'radial'};

ileaves         = 500;                   % Number of interleaves
num_gold_recon  = 100;            % number of interleaves used in golden angle recon

%-----------------------------------
%   generate the cone angle first
cone_area   = 4 * pi / num_gold_recon;              % changeable
cone_angle  = acosd( 1 - cone_area / 2 / pi );

for cone_type   = [5]


    if cone_type == 5
        [R, endpoints] = calc_grmat(ileaves, [0, 1]);
    else
        [R, endpoints] = calc_grmat(ileaves, [-1, 1]);               % golden ratio ordering rotation matrix
    end

    %-----------------------------------
    %   generate the base cones
    [time, base_g, base_k] = gen_base_cone(mat, fov, Ts, gmax, smax, readout_time, cone_angle, cone_type);

    k_traj = rotate( base_k, R(:,:, 1: num_gold_recon) );
    %% -----------------------------------
    %   compute density
    dims = [mat/2, mat/2, mat/2, 1];
    tmpk(:,1,:) = k_traj ./ kmax .* pi;
    Jd = [6, 6, 6];
    Kd = 2 * dims(1:3);
    tmpE = xfm_NUFFT(dims, [], [], tmpk, 'Jd', Jd, 'Kd', Kd, 'PSF', 0); 

    [dens_hist, SNReff] = calc_dens(tmpE.w, sqrt(sum(k_traj.^2, 2)), kmax, 64);
    
    scatter( linspace(0, 1, length(dens_hist)), abs(dens_hist) );
    xlabel('radius, normalized to 0~1'); ylabel('density compensation weight');  ylim([0,8e-3]);
%     title(['base cone type = ', cone_types(cone_type), '  ileaves = ', num2str(ileaves)]);
    
    hold on;
    % y = ones(size(tmpk, 1), 1);
    % PSF = reshape(tmpE'*y, mat/4, mat/4, mat/4);
    
    clear tmpk
end
% legend(char(cone_types(1)),char(cone_types(2)),char(cone_types(3)),char(cone_types(5)));
% drawnow;