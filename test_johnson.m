clear
close all
clc

%----------------------------
%    Define the parameters

% System
gmax    = 5;                        % G/cm
smax    = 15;                       % G/cm/ms
T       = 2e-3;                     % gradient sample time

% Design Parameters
mat     = 32;                       % matrix size
fov     = 256;                      % mm
res     = fov/mat;                  % mm
kmax    = 5 / res;                  % [1/cm]

Ts              = 2e-3;                     % ms
bw_readout      = 1000/Ts;                  % Hz
readout_time    = 2.5;                      % ms
LEN             = floor( readout_time / Ts );


cone_types  = {'Johnson', 'Gurney', 'improvedGurney', 'VDGurney', 'radial'};
cone_type   = 1;

ileaves         = 500;                   % Number of interleaves
num_gold_recon  = 50;            % number of interleaves used in golden angle recon

%-----------------------------------
%   generate the cone angle first
cone_area   = 4 * pi / num_gold_recon;              % changeable
cone_angle  = 3 * acosd( 1 - cone_area / 2 / pi );

[R, endpoints] = calc_grmat(ileaves, [-1, 1]);               % golden ratio ordering rotation matrix

%-----------------------------------
%   generate the base cones
% [time, base_g, base_k] = gen_base_cone(mat, fov, Ts, gmax, smax, readout_time, cone_angle, cone_type);
[time, base_g, base_k] = base_cone1(mat, fov, Ts, gmax, smax, readout_time, cone_angle);
figure;
scatter3(base_k(:,1),base_k(:,2),base_k(:,3)); xlim([-kmax,kmax]); ylim([-kmax,kmax]); zlim([-kmax,kmax]);

k_traj = rotate( base_k, R(:,:, 1: num_gold_recon) );
% -----------------------------------
%   compute density
dims = [mat, mat, mat, 1];
tmpk(:,1,:) = k_traj ./ kmax .* pi;
Jd = [6, 6, 6];
Kd = 2 * dims(1:3);
tmpE = xfm_NUFFT(dims, [], [], tmpk, 'Jd', Jd, 'Kd', Kd); 

[dens_hist, SNReff] = calc_dens(tmpE.w, sqrt(sum(k_traj.^2, 2)), kmax, 64);

figure;
scatter( linspace(0, 1, length(dens_hist)), abs(dens_hist) );
xlabel('radius, normalized to 0~1'); ylabel('density compensation weight');  ylim([0,8e-3]);
%     title(['base cone type = ', cone_types(cone_type), '  ileaves = ', num2str(ileaves)]);

y = ones(size(tmpk, 1), 1);
PSF = reshape(tmpE'*y, dims(1:3));
fwhm = calc_fwhm(abs(PSF));

figure;
surf(1:dims(1), 1:dims(2), abs(PSF(:,:,dims(3)/2)));
figure;
imshow(log(abs(PSF(:,:,ceil(dims(1)/2)))),[],'InitialMagnification','fit');