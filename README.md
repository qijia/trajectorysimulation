# TrajectorySimulation

- Mat = 32 x 32 x 32

Angiograph: 50 times under sampling, 1608 / 50 = 32.16 shots

Full k-space, 10 frames

Perfusion:   25 times under sampling, 1608 / 25 = 64.32 shots

1/3 k-space, 5 frames

- Angle distribution:     not predefined, striclty follow golden ordering
            compare with predefined golden ratio reordering
- Cone angle: 50 shots, ( later try larger cone angle     )
- Ordering: golden     angle, song ordering
- Readout     length: 2.5 / 2e-3 = 1,250 points (     later try longer readout time )

 

Cone condtions

- Johnson

- - Original cost      function
  - weighted cost      function

- Gurney: 

- - Original Gtwist
  - Gtwist adding      weight

- Improved Gurney

- - Original 

- - Tune later

- Radial

- - Original

 

Metrics:

- Density - radius

- SNR efficiency

- Histogram of     weights distribution

- ##### FWHM



## Development of Gurney cones

temporarily failed. Don't know the exact relation between adding the weight `rho` and the generated trajectory.

Try, if manually fixed the rho and still change the `NINT` to get the desired length



`DCF` in the code plays a more important role in changing the shape of the curve
